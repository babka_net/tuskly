#!/bin/bash
set -x
set -e

APP_DIR=/app
VIRTUAL_ENV=/opt/venv
PATH="$APP_DIR:$VIRTUAL_ENV/bin:$PATH"

APP="tuskly.py"
PORT="8080"
APP_DIR="/app"
CMD="$1"

echo "Run Tuskly"

function print_help {
    echo "USAGE:"
    echo "docker run <image> [COMMAND]"
    echo ""
    echo "COMMANDS:"
    echo "web       - Starts the web server."
    echo "shell     - Starts a bash shell."
    echo "*         - Any other command to be passed to the shell."
}

cd ${APP_DIR}

if [[ -z "${CMD}" ]] || [[ "${CMD}" = 'web' ]]; then
    echo "Starting web server..."
    exec streamlit run ${APP} \
         --server.port=${PORT} \
         --server.address=0.0.0.0
elif [[ "${CMD}" = 'shell' ]]; then
    echo "Starting bash shell..."
    exec /bin/bash
elif [[ "${CMD}" = 'help' ]]; then
    print_help
else
    echo "Executing custom command: ${CMD}"
    exec "$@"
fi
