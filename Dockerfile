FROM python:3.11-slim as python

ENV VIRTUAL_ENV=/opt/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="/app:$VIRTUAL_ENV/bin:$PATH"
WORKDIR /app

# Install dependencies:
COPY requirements.txt .
RUN pip install -r requirements.txt

# Run the application:
COPY . ./

HEALTHCHECK CMD curl --fail http://localhost:8080/_stcore/health

EXPOSE 8080

ENTRYPOINT ["/app/entrypoint.sh"]
